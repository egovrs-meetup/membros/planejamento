# Objetivos
- Criar uma comunidade local de servidores públicos interessados na gestão de TI.
- Possibilitar a identificação de oportunidades de troca de experiências e soluções entre órgãos.
- Dar voz àqueles que, embora não sejam nomeadamente responsáveis pela TI nos órgãos, atuam de forma pró-ativa no desenvolvimento de soluções e na melhoria do ambiente.
- Incentivar o empreendedorismo nas TIs dos órgãos públicos.

# Público-alvo
>Servidores efetivos de órgãos públicos do RS, não necessariamente gestores de TI, mas interessados na gestão de recursos, projetos e processos de TI em seus respectivos órgãos.

# Áreas de interesse
- Compartilhamento de experiências e tecnologias
- Software livre
- Redução de custos
- Processos e pessoas
- Inovação: o uso de novas tecnologias para resolver velhos problemas
- Governo digital
- Transparência e dados abertos 
- Empreendedorismo
- Futuro da TI no governo

# Encontros
- Agendados via [GetTogether](https://gettogether.community/egov-rs/).
- Apresentação de talks.
- Networking.

# Organizadores
- [Fábio Tramasoli](mailto:tramasoli@mprs.mp.br) - MP/RS
- [Flávio Knob](mailto:ffknob@tce.rs.gov.br) - TCE/RS

# Contato
- Telegram: [eGov-RS Meetup](https://t.me/joinchat/E3uFxU5O3oBhmMRaxJRNWA)